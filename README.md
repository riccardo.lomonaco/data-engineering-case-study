# data-engineering-case-study

## Instructions and overview
I decided to use Docker to host and run the case study.

I created two docker images:
1. MySQL database
2. FastAPI webserver

The idea is that the PM can make requests to the FastAPI webserver to access the daily data and historic data from the h8 data, this includes the simple moving averages and the difference.

Because of the time constraint however, I was not able to complete the setup of the Dockerfile and docker-compose to have a functional container, so when trying to run the docker-compose, it will not work. Because of this I was also not able to complete the sql_client.py file which aimed at interacting with the SQL database.

Ideally you would run the docker-compose file and then simply make a request to the FastAPI webserver for the necessary data.