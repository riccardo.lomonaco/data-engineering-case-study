"""Main entrypoint to fast api webserver"""

from fastapi import FastAPI
import uvicorn
from vendors.federal_reserve import FederalReserve
from transform.derive import DerivedData
import logging

app = FastAPI()
logger = logging.getLogger("DataTransformer")
logger.setLevel("INFO")


@app.post("/download_daily")
def retrieve_daily_data():
    """
    Retrieve daily data from federal reserve system
    :return: Status message with successful write to MySQL as well as sql table statement
    """
    # Begin by updating data
    federal_reserve_client = FederalReserve()
    load_response = federal_reserve_client.retrieve_daily_data()

    # Then regenerate derived data
    derived_data_client = DerivedData()
    derive_response = derived_data_client.generate_derived_data()

    logger.info(f"Obtained the following responses - federal data update: "
                f"{load_response}, derived data generation: {derive_response}")


@app.post("/download_historic")
def retrieve_historic_data():
    """
    Retrieve historic data from federal reserve system
    :return: Status message with successful write to MySQL as well as sql table statement
    """
    # Begin by updating data
    federal_reserve_client = FederalReserve()
    load_response = federal_reserve_client.retrieve_historic_data()

    # Then regenerate derived data
    derived_data_client = DerivedData()
    derive_response = derived_data_client.generate_derived_data()

    logger.info(f"Obtained the following responses - federal data update: "
                f"{load_response}, derived data generation: {derive_response}")


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0")
