"""Class to prepare derived data given latest sql data"""

import pandas as pd

from base.sql_client import SQLClient


class DerivedData(SQLClient):
    """
    Class to prepare derived data given latest sql data
    """
    def __init__(self):
        super(DerivedData, self).__init__()
        self.input_data_table = "h8_raw"
        self.derived_data_table_name_1 = "h8_derived_sma"
        self.derived_data_table_name_2 = "h8_derived_diff"

    def generate_derived_data(self) -> str:
        """
        Prepare derived data and update SQL table
        :return: String with MYSQL operation result
        """
        # Retrieve data
        df = self.get_table(self.input_data_table)

        # Apply derived calculations
        # Start by generating moving averages
        sma_1 = df['Real estate loans, all commercial banks, seasonally adjusted'].rolling(40).mean()
        sma_2 = df[
            'Other securities: Mortgage-backed securities, large domestically chartered commercial banks, seasonally' \
            ' adjusted'].rolling(40).mean()
        sma_df = pd.concat([sma_1, sma_2], axis=1)

        # Now calculate the differences
        df['Cash Assets all commercial banks, seasonally adjusted, difference week on week'] = df[
            'Cash assets, all commercial banks, seasonally adjusted'
        ].diff()

        # Finally recreate the two tables
        # Delete derived table
        self.delete_table(self.derived_data_table_name_1)
        self.delete_table(self.derived_data_table_name_2)

        # Create new derived table
        derived_response_1 = self.create_table(self.derived_data_table_name_1, sma_df)
        derived_response_2 = self.create_table(self.derived_data_table_name_2, df)

        return f"Tables published to MySQL with following status: [" \
               f"{self.derived_data_table_name_1}: {derived_response_1}, " \
               f"{self.derived_data_table_name_2}: {derived_response_2}]"
