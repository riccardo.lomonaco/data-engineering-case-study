"""Client to interact with MySQL"""

import pandas as pd
from sqlalchemy import create_engine, text

class SQLClient:
    """
    Class to interact with MySQL
    """

    def _init_(self):
        self.conn_string = "mysql+pyodbc://root:p@ssw0rd1@localhost:3306/sqlalchemy"
        self.engine = create_engine(self.conn_string)
        with self.engine.connect() as connection:
            connection.execute(text("CREATE DATABASE IF NOT EXIST fed_db;"))

    def check_table(self, table_name: str) -> bool:
        """
        Check if table exists
        :param table_name: Name of table to verify
        :return: Bool whether table exists
        """
        with self.engine.connect() as connection:
            check_table = connection.execute(text(f"SELECT * FROM {table_name};"))
        
        # Add logic to decide if check should return true or false
        pass


    def insert_into_table(self, table_name: str, df: pd.DataFrame) -> str:
        """
        Insert records into existing MySQL table
        :param table_name: Name of table requiring insert
        :param df: Data to be inserted
        :return: String with response from operation
        """

        with self.engine.connect() as connection:
            check_table = connection.execute(text(f"INSERT INTO {table_name} ();"))

        # If insert is successful reutrn a successful string if not return error
        pass

    def create_table(self, table_name: str, df: pd.DataFrame) -> str:
        """
        Create new table and insert MySQL records
        :param table_name: Name of table requiring creation
        :param df: Data to be inserted into newly created table
        :return: String with response from operation
        """
        # Create the table with the correct column names and types taken from the source data
        with self.engine.connect() as connection:
            connection.execute(text(f"CREATE TABLE {table_name} ();"))
        
        # If table has been created successfully return a successful string if not return error
        pass

    def delete_table(self, table_name: str) -> str:
        """
        Delete table
        :param table_name: Name of table requiring deletion
        :return: String with response from operation
        """

        # Deleting the selected table
        with self.engine.connect() as connection:
            connection.execute(text(f"DROP TABLE {table_name};"))

        # If table has been deleted successfully return a successful string if not return error
        pass

    def get_table(self, table_name: str) -> pd.DataFrame:
        """
        Given a table name return table as a dataframe
        :param table_name: Name of table to retrieve
        :return: Dataframe table
        """

        # Select all rows from the selected table
        with self.engine.connect() as connection:
            table = connection.execute(text(f"SELECT * FROM {table_name};"))
        
        # Return the selected table as a Pandas df
        pass





