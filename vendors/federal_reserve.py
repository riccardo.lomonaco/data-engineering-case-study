"""Class to retrieve data from federal reserve website"""

import pandas as pd
from copy import copy
import requests
from datetime import datetime, timedelta
from base.sql_client import SQLClient
from io import StringIO
from typing import Tuple


class FederalReserve(SQLClient):
    """
    Class to retrieve data from federal reserve website
    """

    def __init__(self):
        super(FederalReserve, self).__init__()
        # Initialise the request session
        self.request_session = requests.session()

        # Initialise formattable url for federal reserve
        self.federal_reserve_url = "https://www.federalreserve.gov/datadownload/Output.aspx?rel=H8&series="\
            "8789dda98817a5682f19fb7e8093e669&lastobs=&from={}&to={}&filetype"\
            "=csv&label=include&layout=seriescolumn"

        # SQL Table names
        self.metadata_table_name = ""
        self.data_table_name = ""

    @staticmethod
    def format_df(data: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Format federal reserve data to required format
        :param data: data requiring formatting
        :return: formatted data
        """
        # Extract metadata
        metadata = data[:5]

        # Remove metadata from main dataset
        data = data[5:]

        return data, metadata

    def retrieve_daily_data(self) -> str:
        """
        Retrieve daily data from the federal reserve website
        :return: String with MYSQL operation result
        """
        # Start by retrieving the daily data

        # Find dates for URL formatting
        today = datetime.now().date()
        yesterday = (today + timedelta(-1)).strftime("%m/%d/%Y")
        today = today.strftime("%m/%d/%Y")

        # Construct daily URL
        daily_url = copy(self.federal_reserve_url.format(yesterday, today))

        # Get the data (this will be a csv buffer)
        daily_io_data = self.request_session.get(daily_url).content
        daily_data = pd.read_csv(StringIO(daily_io_data.decode('utf-8')))

        # Format daily data
        daily_df_metadata, daily_df_data = self.format_df(daily_data)

        # Create tables in SQL
        # Metadata
        self.delete_table(self.metadata_table_name)
        metadata_response = self.create_table(self.metadata_table_name, daily_df_metadata)

        # Data
        data_response = self.insert_into_table(self.data_table_name, daily_df_data)

        return f"Tables published to MySQL with following status: [{self.metadata_table_name}: {metadata_response}, " \
               f"{self.data_table_name}: {data_response}]"

    def retrieve_historic_data(self) -> str:
        """
        Retrieve historic data from the federal reserve website
        :return: String with MYSQL operation result
        """
        # There is a maximum number of observations per request, min year is 1973 we prepare the URLS iteratively
        max_year = datetime.now().year
        min_year = 1973
        federal_reserve_urls = [
            self.federal_reserve_url.format(f"01/01/{year}", f"01/01/{year+17}")
            if year > (max_year - 17)
            else
            self.federal_reserve_url.format(f"01/01/{year}", f"01/01/{max_year}")
            for year in range(min_year, max_year + 1, 17)
        ]

        # Now loop through each and prep historic df
        historic_dfs = []
        previous_historic_metadata = pd.DataFrame()
        for url in federal_reserve_urls:

            # Get the data (this will be a csv buffer)
            historic_io_data = self.request_session.get(url).content
            historic_data = pd.read_csv(StringIO(historic_io_data.decode('utf-8')))

            # Format the data
            historic_df_metadata, historic_df_data = self.format_df(historic_data)

            # Check if metadata was previously stored
            if not previous_historic_metadata.empty:
                previous_historic_metadata = pd.concat([previous_historic_metadata, historic_df_metadata]
                                                       ).drop_duplicates()
            # Append formatted data
            historic_dfs.append(copy(historic_df_data))

        # Concatenate output
        historic_df = pd.concat(historic_dfs)

        # Create tables in SQL
        # Metadata
        self.delete_table(self.metadata_table_name)
        metadata_response = self.create_table(self.metadata_table_name, previous_historic_metadata)

        # Data
        data_response = self.create_table(self.data_table_name, historic_df)

        return f"Tables published to MySQL with following status: [{self.metadata_table_name}: {metadata_response}, " \
               f"{self.data_table_name}: {data_response}]"
